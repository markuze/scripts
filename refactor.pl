#!/usr/bin/perl -w

use strict;

use constant false => 0;
use constant true  => 1;

my $file = "intel-iommu.c"; #$ARGV[0];
my ($rm, $mv);
my ($ofh, $ifh);
my @out         = ();
my @outc        = ();

sub exclude_line {
    my $ex = shift;
    if ($ex =~ /^\#|[=><!]=/) { return 1} else {return 0}
}

if ($^O =~ /Win/) {
        $mv = "move";
        $rm = "del";
} else {
        $mv = "mv";
        $rm = "rm -f";
}

sub parse_block {

    my $block       = shift;
    my $block_eq    = "";
    my $offset      = 0;
    my $count       = 0;
    my $mcount      = 0;
    #find longest string coming before =
    foreach my $line (split(/\n/, $block)) {
        $count++;
        next if (exclude_line($line));
        $line =~ /\s*(\W{1}=)\s*/;
        my @parts   = split(/\s*\W{1}=\s*/, $line);
        my $len     = length ($parts[0]);

        $len++ if (defined($1));

        if ($len > $offset) {
            $offset = $len;
        }
        $mcount++;
    }

    push @out, $block and return if ($count == 1);
    
    #Tabify multiline, not needed for one liner.
    $offset += $offset % 4 if ($mcount > 1);

    #Allign before and after =
    foreach my $line (split(/\n/, $block)) {
        push @out, "$line\n" and next if (exclude_line($line));
        $line =~ /\s*(\W{1})=\s*/;
        my @parts   = split(/\s*\W{1}=\s*/, $line);
        my $len     = length ($parts[0]);
        my $match   = "=";

        #$match = $1 and $block_eq .= "// match [$1]\n" if (defined($1));
        $match = "${1}=" if (defined($1) and not ("$1" =~ /\s/));
        $block_eq  = "$parts[0]";
        $block_eq .= " " x ($offset - $len);
        $block_eq .= "$match $parts[1]\n";
        push @out, $block_eq;
    }
}

sub parse_block_coma {

    my $block_r     = shift;
    my $block_eq    = "";
    my @offset      = ();
    my $count       = 0;

    #find longest string coming before =
    foreach my $line (@{$block_r}) {

        my @parts   = split(/\s*,\s*/, $line);

        foreach my $i (0 .. $#parts) {
            $offset[$i] = length($parts[$i]) if (not defined($offset[$i]) or $offset[$i] < length($parts[$i]));
        }
        $count++;
    }

    push @outc, "@{$block_r}" and return if ($count == 1);

    #Allign before and after =
    foreach my $line (@{$block_r}) {

        my @parts   = split(/\s*,\s*/, $line);
        foreach my $i (0 .. $#parts -1) {
            my $len     = length ($parts[$i]);

            $block_eq .= $parts[$i];
            $block_eq .= " " x ($offset[$i] - $len);
            $block_eq .= ", ";
        }
        $block_eq .= $parts[$#parts];
        push @outc, "$block_eq";
        $block_eq = "";
    }
}

sub parse_block_filter_coma {

        my $block_r       = shift;
        my @l_block     = ();
        my $len;

        if ($#{$block_r} == 0) {
            push @outc ,"@{$block_r}";
            @{$block_r} = ();
            return
        }
        foreach my $line (@{$block_r}) {
            $line =~ /^(.*)(\(.*\);)$/;
 #block line is valid only in this form ...(..,..);
 #flush the buffer and print the line
            if (not (defined($1) and defined($2))) {
                parse_block_coma \@l_block if ($#l_block > -1);
                push @outc, $line;
                @l_block = ();
                undef $len;
                next;
            }
            chomp $line;
            printf ("$line <$1><$2>\n");
            $line = "$line\n";
            $len        = index($line, '(') unless defined($len);

            if ($len != index($line, '(')) {
                parse_block_coma \@l_block;
                @l_block = ("$line");
                $len = index($line, '(');
            } else {
                push @l_block , "$line";
            }
        }
        parse_block_coma \@l_block if ($#l_block > -1);
        @{$block_r} = ();
}

unless (defined($file)) {
    printf "$#ARGV @ARGV\n";
    die "Input variables missing";
}

printf "$file\n";

system("$mv $file ${file}__");

open($ifh, '<', "${file}__") or die "Could not open $file due to $!";
open($ofh, '>', "$file") or close $ifh and die "Could not open $file due to $!";

my $block       = "";
my $len         = -1;

## Aligning on =
foreach my $line (<$ifh>) {
    if ($line =~ /=/){

        $line   =~ /^(\s*)/;
        $len = length($1) if ($len == -1);

        if ($len != length($1)) {
            parse_block($block);
            $block = $line;
        } else {
            $block .= $line;
        }
        next;
    }
    if (length($block)) {
        parse_block($block);
        $block = "";
        $len = -1;
    }
    push @out, $line;
}

my @block       = "";
undef $len;

## Aligning on ,
foreach my $line (@out) {

# collect all consec lines with same indent that contain ,
    if ($line =~ /,/){
        $line   =~ /^(\s*)/;
        $len = length($1) unless defined($len);

        if ($len != length($1)) {
            parse_block_filter_coma(\@block);
            @block = ("$line");
            $len = length($1);
        } else {
            push @block, $line;
        }
        next;
    }

    if ($#block >= 0) {
        parse_block_filter_coma(\@block);
        $block = ();
        undef $len;
    }
    push @outc, "$line";
}

foreach my $line (@outc) {
    print $ofh "$line";
}


close $ifh;
close $ofh;
system("$rm ${file}__");

